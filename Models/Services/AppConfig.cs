﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserFrontEnd.Models.Services
{
    public class AppConfig
    {
        public string Secret { get; set; }
    }
}
