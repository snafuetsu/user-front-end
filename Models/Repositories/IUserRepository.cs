﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserFrontEnd.Models.Repositories
{
    public interface IUserRepository
    {
        public User Create(User user);
        public User Read(int id);
        public ICollection<User> ReadAll();
        public void Update(int id, User User);
        public void Delete(int id);
    }
}
