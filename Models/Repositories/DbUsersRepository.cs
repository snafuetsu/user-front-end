﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserFrontEnd.DatabaseContext;

namespace UserFrontEnd.Models.Repositories
{
    public class DbUsersRepository : IUserRepository
    {
        private UserDbContext _db;

        public DbUsersRepository(UserDbContext db)
        {
            _db = db;
        }

        public User Create(User user)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public User Read(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<User> ReadAll()
        {
            throw new NotImplementedException();
        }

        public void Update(int id, User User)
        {
            throw new NotImplementedException();
        }
    }
}
