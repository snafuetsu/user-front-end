﻿"use strict";
//var category = "business";

/*
 * purpose: the ready function for the document
 * 
 * author: Taylor Cox
 */
$(document).ready(function () {
    /*var country = "us";
    var category = "business";
    var apiKey = "4e79c613dd4944509135c764a944d537"
    var inURL = generateURL(country, category, apiKey);*/

    //var page = "1";
    //var pagesize = "3";

    //var inURL = generateURL(page, pagesize);

    var inURL = generateURL();



    /*
     * purpose: a function to generate a url 
     * 
     * author: Taylor Cox
     */
    function generateURL() {
        var url = "http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/Category";
        return url;
    }

    function getPage(inURL) {
        $.ajax({
            url: inURL,
            beforeSend: function () {
                $("#outputArea").html('<img src="' + '/images/ajax-loader.gif' + '">'); //displays this before sending the ajax call
            },
            success: function (response) {
                response.data.forEach(category => {

                    //displays the category of the entry
                    $('#name').html(category.name);

                    //displays the value of the entry
                    $('#units').html(category.units);

                    //displays the timeStamp of the entry 
                    $('#description').html(category.description);

                });
            },
            error: function _error() {
                //inserts an error message into the element with id cagegoryRows
                $('#categoryRows').html('Project not found');
            }
        });
    }
})