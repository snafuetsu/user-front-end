﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UserFrontEnd.Models;

namespace UserFrontEnd.Controllers
{
    public class HomeController : Controller {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult ProjectsList(int PageNum)
        {
            ViewBag.PageNum = PageNum;
            return View();
        }
        public IActionResult DeleteProject(string ProjectId, string ProjectName)
        {
            ViewBag.ProjectId = ProjectId;
            ViewBag.ProjectName = ProjectName;
            return View();
        }
        public IActionResult DeleteCategory(string CategoryId, string CategoryName, string ProjectId)
        {
            ViewBag.CategoryId = CategoryId;
            ViewBag.CategroyName = CategoryName;
            ViewBag.ProjectId = ProjectId;
            return View();
        }
        public IActionResult DeleteEntry(string CategoryId, string EntryId)
        {
            ViewBag.CategoryId = CategoryId;
            ViewBag.EntryId = EntryId;
            return View();
        }
        [HttpPost]
        public IActionResult CreateCategory(string ProjectId, string Name, string Description, string Units)
        {
            string url = "http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/Category";
            string returnUrl = "CategoriesList?ProjectId=" + ProjectId + "&pageNum=1";
            HttpClient client = new HttpClient();
            var content = new FormUrlEncodedContent(new []
            {
                new KeyValuePair<string, string>("Name", Name),
                new KeyValuePair<string, string>("Description", Description),
                new KeyValuePair<string, string>("Units", Units),
                new KeyValuePair<string, string>("Project", ProjectId)
            });
            var response = client.PostAsync(url, content);
            return Redirect(returnUrl);
        }
        [HttpPost]
        public IActionResult CreateEntryPut(string CategoryId, string Value)
        {
            DateTime localDate = DateTime.Now;
            string url = "http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/Entry";
            string returnUrl = "EntriesList?CategoryId="+CategoryId+"&pageNum=1";

            HttpClient client = new HttpClient();
            var content = new FormUrlEncodedContent(new []
            {
                new KeyValuePair<string, string>("Timestamp", localDate.ToString()),
                new KeyValuePair<string, string>("Category", CategoryId),
                new KeyValuePair<string, string>("Value", Value)
            });
            var response = client.PostAsync(url, content);
            return Redirect(returnUrl);
        }
        public IActionResult CreateEntry(string CategoryId)
        {
            ViewBag.CategoryId = CategoryId;
            return View();
        }
        public IActionResult CreateCategory(string ProjectId)
        {
            ViewBag.ProjectId = ProjectId;
            return View();
        }
        public IActionResult CreateProject(string ProjectId)
        {
            ViewBag.ProjectId = ProjectId;
            return View();
        }

        [HttpPost]
        public IActionResult CreateProject(string name, string description)
        {
            string url = "http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/Project";
            HttpClient client = new HttpClient();
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("Name", name),
                new KeyValuePair<string, string>("Description", description)
            });
            var response = client.PostAsync(url, content);
            return Redirect("ProjectsList");
        }
        public IActionResult UpdateProject(string ProjectId, string Name, string Description)
        {
            ViewBag.Id = ProjectId;
            ViewBag.Name = Name;
            ViewBag.Description = Description;

            return View();
        }

        [HttpPost]
        public IActionResult UpdateProjectPut(string ProjectId, string Name, string Description)
        {
            string url = "http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/Project/" + ProjectId;
            string returnUrl = "ProjectsList";
            HttpClient client = new HttpClient();

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("id", ProjectId),
                new KeyValuePair<string, string>("Id", ProjectId),
                new KeyValuePair<string, string>("Name", Name),
                new KeyValuePair<string, string>("Description", Description)
            });
            var response = client.PutAsync(url, content);

            return Redirect(returnUrl);
        }

        [HttpPost]
        public IActionResult UpdateCategoryPut(string ProjectId, string id, string Name, string Description, string Units)
        {
            string url = "http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/Category/" + id;
            string returnUrl = "CategoriesList?ProjectId=" + ProjectId + "&pageNum=1";
            HttpClient client = new HttpClient();
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("Name", Name),
                new KeyValuePair<string, string>("Description", Description),
                new KeyValuePair<string, string>("Units", Units),
                new KeyValuePair<string, string>("Project", ProjectId)
            });
            var response = client.PutAsync(url, content);
            return Redirect(returnUrl);
        }

        public IActionResult UpdateCategory(string ProjectId, string CategoryName, string CategoryDescription, string CategoryUnits, string CategoryId)
        {
            ViewBag.ProjectId = ProjectId;
            ViewBag.CategoryName = CategoryName;
            ViewBag.CategoryDescription = CategoryDescription;
            ViewBag.CategoryId = CategoryId;
            ViewBag.CategoryUnits = CategoryUnits;
            return View();
        }

        public IActionResult CategoriesList(string ProjectId, int PageNum)
        {
            ViewBag.ProjectId = ProjectId;
            ViewBag.PageNum = PageNum;
            return View();
        }

        public IActionResult EntriesList(string CategoryId, int PageNum)
        {
            ViewBag.CategoryId = CategoryId;
            ViewBag.PageNum = PageNum;
            return View();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
