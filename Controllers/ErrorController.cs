﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace UserFrontEnd.Controllers
{
    [Route("error")]
    public class ErrorController : Controller
    {
        //action method for the 404 error page
        [Route("404")]
        public IActionResult PageNotFound()
        {
            return View();
        }

        //action method for the 500 error page
        [Route("500")]
        public IActionResult ErrorOccured()
        {
            return View();
        }
    }
}