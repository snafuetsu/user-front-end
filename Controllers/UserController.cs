﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserFrontEnd.Models;
using UserFrontEnd.Models.Repositories;
using UserFrontEnd.Models.Services;

namespace UserFrontEnd.Controllers
{
    public class UserController : Controller
    {
        private IUserRepository _repo;
        private IUserService _userService;

        public UserController(IUserRepository repo, IUserService userService)
        {
            _repo = repo;
            _userService = userService;
        }

        public IActionResult Index()
        {
            var model = _repo.ReadAll();
            return View(model);
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                _repo.Create(user);
                return RedirectToAction("Index");
            }
            return View(user);
        }

        public IActionResult Edit(int id)
        {
            var user = _repo.Read(id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            return View(user);
        }

        [HttpPost]
        public IActionResult Edit(Models.User user)
        {
            if (ModelState.IsValid)
            {
                _repo.Update(user.Id, user);
                return RedirectToAction("Index");
            }
            return View(user);
        }

        public IActionResult Details(int id)
        {
            var user = _repo.Read(id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            return View(user);
        }

        public IActionResult Delete(int id)
        {
            var user = _repo.Read(id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            _repo.Delete(id);
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]User userParam)
        {
            var user = _userService.Authenticate(userParam.UserName, userParam.Password);

            if(user == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }

            return Ok(user);
        }

        [HttpPost]
        public JsonResult PostUser(User user)
        {
            Debug.Assert(user != null);
            return Json(user);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }
       
    }
}